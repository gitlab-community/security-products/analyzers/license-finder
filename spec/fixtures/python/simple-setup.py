#!/usr/bin/env python3
from setuptools import find_packages
from setuptools import setup

setup(
    name="package name",
    version='1.1',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        "boto3",
    ],
    author="author",
    author_email="author@author.com",
    description="All the stuff",
    url="https://www.author.com",
)
