# frozen_string_literal: true

ruby_version = ENV.fetch('RUBY_VERSION', '2.7.2')

name "ruby-#{ruby_version}"
maintainer "GitLab B.V."
homepage "https://www.ruby-lang.org/"

install_dir "/opt/asdf/installs/ruby/#{ruby_version}"
package_scripts_path Pathname.pwd.join("config/scripts/ruby")

build_version ruby_version
build_iteration 1

override 'asdf_ruby', version: ruby_version
dependency "asdf_ruby"

package :deb do
  compression_level 9
  compression_type :xz
end
