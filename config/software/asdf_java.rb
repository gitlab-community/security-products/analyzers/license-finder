# frozen_string_literal: true

name "asdf_java"
default_version "11"

dependency "asdf_maven"
dependency "asdf_gradle"
dependency "zlib"

whitelist_file "bin"
whitelist_file "lib"
whitelist_file "jre/bin"
whitelist_file "jre/lib"

version "8" do
  relative_path "jdk8u262-b10"
  source(
    url: "https://github.com/AdoptOpenJDK/openjdk8-binaries/releases/download/jdk8u262-b10/OpenJDK8U-jdk_x64_linux_hotspot_8u262b10.tar.gz",
    sha256: "733755fd649fad6ae91fc083f7e5a5a0b56410fb6ac1815cff29f744b128b1b1"
  )
end

version "11" do
  relative_path "jdk-11.0.8+10"
  source(
    url: "https://github.com/AdoptOpenJDK/openjdk11-binaries/releases/download/jdk-11.0.8%2B10/OpenJDK11U-jdk_x64_linux_hotspot_11.0.8_10.tar.gz",
    sha256: "6e4cead158037cb7747ca47416474d4f408c9126be5b96f9befd532e0a762b47"
  )
end

version "14" do
  relative_path "jdk-14.0.2+12"
  source(
    url: "https://github.com/AdoptOpenJDK/openjdk14-binaries/releases/download/jdk-14.0.2%2B12/OpenJDK14U-jdk_x64_linux_hotspot_14.0.2_12.tar.gz",
    sha256: "7d5ee7e06909b8a99c0d029f512f67b092597aa5b0e78c109bd59405bbfa74fe"
  )
end

version "15" do
  relative_path "jdk-15.0.2+7"
  source(
    url: "https://github.com/AdoptOpenJDK/openjdk15-binaries/releases/download/jdk-15.0.2%2B7/OpenJDK15U-jdk_x64_linux_hotspot_15.0.2_7.tar.gz",
    sha256: "94f20ca8ea97773571492e622563883b8869438a015d02df6028180dd9acc24d"
  )
end

build do
  mkdir install_dir
  copy "#{project_dir}/**", "#{install_dir}/"
  delete "#{install_dir}/lib/ext"
  delete "#{install_dir}/man"
  delete "#{install_dir}/sample"
  delete "#{install_dir}/src.zip"
end
